FROM sashimi/nodejs:latest

MAINTAINER Ziggo Software

# Install yarn as an alternative to npm.
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb http://dl.yarnpkg.com/debian/ stable main" >> /etc/apt/sources.list.d/yarn.list \
  && apt-get update \
  && apt-get install -y yarn

# Add all the sources to the app directory.
COPY src /usr/src/app

# Switch to the app directory
WORKDIR /usr/src/app

# Install all dependencies.
RUN yarn install

CMD ["npm", "start"]
