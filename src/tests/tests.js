const assert = require('assert');

describe('math still works', () => {
    it('1 + 1 is still 2', () => {
        assert.equal(1 + 1, 2);
    });
});
